<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Listener extends Command
{
    protected $name = 'rabbitmq:listener';
    public function handle()
    {
        \Amqp::consume('image_service_flush_redis_queue', function ($message, $resolver) {
            $data=$message->body;
            $data= unserialize($data);
            if($data[0] == "flush_redis_key"){
                \Redis::del($data[1]);
            }

           // var_dump($message->body);
            $resolver->acknowledge($message);
        }, [
            'exchange' => 'amq.fanout',
            'exchange_type' => 'fanout',
            'queue_force_declare' => true,
            'queue_exclusive' => false,
            'persistent' => true// required if you want to listen forever
        ]);
    }
}
