<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('settings', 'App\Http\SettingsFacade');
    }

    public function boot(){
        /**
         * takes the google cloud credential
         */
        $array = [
            "type" =>env('STACK_TYPE') ,
            "project_id" => env('STACK_PROJECT_ID'),
            "private_key_id" => env('STACK_PRIVATE_KEY_ID'),
            "private_key" => str_replace('\n', "\n", env('STACK_PRIVATE_KEY')),
            "client_email" => env('STACK_CLIENT_EMAIL'),
            "client_id" => env('STACK_CLIENT_ID'),
            "auth_uri" => env('STACK_AUTH_URI'),
            "token_uri" => env('STACK_TOKEN_URL'),
            "auth_provider_x509_cert_url" => env('STACK_AUTH_PROVIDER_x509_CERT_URL'),
            "client_x509_cert_url" => env('STACK_CLIENT_X509_CERT_URL')
        ];
        /**
         * the path to create and store the json
         */
        $path = storage_path().'/stackdriver.json';
        /**
         * open the file
         */
        $fp = fopen($path, 'w');
        /**
         * store array in json with pretty print
         */
        fwrite($fp, json_encode($array,JSON_PRETTY_PRINT ));
        fclose($fp);
    }
}
