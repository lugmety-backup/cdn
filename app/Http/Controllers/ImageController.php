<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/29/2017
 * Time: 3:30 PM
 */

namespace App\Http\Controllers;

use Google\Cloud\Core\Exception\GoogleException;
use Google\Cloud\Storage\StorageClient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Image;
use Illuminate\Support\Facades\File;
use Symfony\Component\Debug\Exception\FatalErrorException;

/**
 * Class ImageController
 * @package App\Http\Controllers
 */
class ImageController extends Controller
{

    /**
     * store image and return its path
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(Request $request){

        try{
            $this->validate($request,[
                "type" => "required|min:1",
                "avatar" => "required"
            ]);
        }
        catch(\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
//        return $request->all();
        try{
            $base_path=ENV("IMAGE_BASE_PATH");
            $returrnImageBaseUrl = explode("/",$base_path,3);
            $uniqueRandomNumber = $this->generateRandomNumber();
            $imageName = $uniqueRandomNumber.uniqid().".png";
            $avatar = $request->type;
            $imageData = $request->avatar;
            if(!$imageData === base64_encode(base64_decode($imageData))){
                throw new \Exception();
            }
            $img = Image::make(file_get_contents($imageData))->save(base_path().$base_path.$imageName);
            $name1 = base_path("/public/images/$imageName");

            $path = "/".$returrnImageBaseUrl[2].$imageName;
            if(isset($request["service"])){
                if($request->service =="food") {
                    $normalImageName = "normal-".$imageName;
                    /** @var  $width & @var  $height is retrived from settings*/
                    $data = \Settings::getSettings('image-service-normal-image-size');
                    if ($data["status"] != 200) {
                        return response()->json(
                            $data["message"]
                            , $data["status"]);
                    }
                    $width = $data['message']['data']['value'][0];
                    $height = $data['message']['data']['value'][1];
                    $width = Config::get('config.image_width');
                    $height = Config::get('config.image_height');
                    $norm= Image::make(file_get_contents($imageData))->resize($width,$height, function ($c) {
                        $c->aspectRatio();
                        $c->upsize();
                    })->save(base_path() . $base_path . $normalImageName);
                    $name = base_path("/public/images/$normalImageName");
                    $norm = "/".$returrnImageBaseUrl[2].$normalImageName;
                }
            }

            return response()->json([
                "status" => "200",
                "data" => $path
            ],200);
        }
        catch(GoogleException $ex ){
	        return response()->json([
		        'status'=>'422',
		        'message'=>$ex->getMessage()
	        ],422);
//            $resumeUri = $uploader->getResumeUri();
//            $uploader->resume($resumeUri);
        }
        catch (\Exception $exception){
            return response()->json([
                'status'=>'422',
                'message'=>['avatar'=>["The $avatar must be in valid Base64 image format"]]
            ],422);
        }
    }

    /*
     * generate random number
     */
    /**
     * @return int
     */
    private function generateRandomNumber(){
        return  mt_rand();
    }

    public function create(){
        return view("image_form");
    }

    public function store(Request $request){
        $base_path=ENV("IMAGE_BASE_PATH");
        $returrnImageBaseUrl = explode("/",$base_path,3);
        $uniqueRandomNumber = $this->generateRandomNumber();
        $imageName=$uniqueRandomNumber.uniqid().".png";
        $img=Image::make($request->pic)->save(base_path().$base_path.$imageName);

        $normalImageName = "normal-".$imageName;
        /** @var  $width & @var  $height is retrived from settings*/

        $width = 250;
        $height = 250;
//                    $width = Config::get('config.image_width');
//                    $height = Config::get('config.image_height');
        $norm= Image::make($request->pic)->resize($width,$height, function ($c) {
            $c->aspectRatio();
            $c->upsize();
        })->save(base_path() . $base_path . $normalImageName);
        return response()->json(["http://api.stagingapp.io/cdn/v1/images/$imageName","http://api.stagingapp.io/cdn/v1/images/$normalImageName"]);

    }
}